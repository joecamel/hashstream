import os
import urllib2
import argparse
from poster.encode import multipart_encode
from poster.streaminghttp import register_openers

desc = "Upload photos from a directory to Hash Stream."

argp = argparse.ArgumentParser(description=desc)
argp.add_argument("--url", help="url of the upload page")
argp.add_argument("--dir", help="directory with photos")
argp.add_argument("--tag", help="hash tag used")
argp.add_argument("--geo", help="geo coordinates, separated by comma")

args = argp.parse_args()

register_openers()

if args.geo:
  lat, lon = args.geo.split(',')
else:
  lat, lon = None, None

for filename in os.listdir(args.dir):
  datagen, headers = multipart_encode({
    "photo": open(os.path.join(args.dir, filename), "rb"),
    "tag": args.tag,
    "lat": lat,
    "lon": lon,
  })
  request = urllib2.Request(args.url, datagen, headers)
  urllib2.urlopen(request).read()
  print 'Uploaded %s' % filename
