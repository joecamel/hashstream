import datetime
import flask
from flask import render_template, request, Response, redirect, url_for
from flask import jsonify
from flask.ext.cache import Cache
from werkzeug.exceptions import NotFound
from sqlalchemy import sql

import photos
import models
import utils
from models import db, db_session, Photo
from utils import valid_tag, normalize_tag, send_post

app = flask.Flask(__name__)
app.config.from_object('config')

cache = Cache(app)

# Filters
@app.template_filter('photo_url')
def photo_url(digest):
    return photos.get_photo_url(digest)

@app.template_filter('thumbnail_url')
def thumbnail_url(digest):
    return photos.get_thumbnail_url(digest)

# Enables automatic teardown of db session
@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()

# Cached functions

@cache.memoize(app.config['CACHE_DEFAULT_TIMEOUT'])
def get_latest_tags():
  app.logger.info('Miss: get_latest_tags')
  tags = models.get_latest_tags(app.config['REFRESH_PERIOD'])
  return tags

# Views
@app.route("/")
def list_tags():
  tags = get_latest_tags() # cached function

  try:
    android = request.user_agent.platform.lower() == 'android'
  except:
    android = False

  featured = {
      'protothon': models.get_latest_photos('protothon', 3),
      'ish': models.get_latest_photos('ish', 3),
      'pariz': models.get_latest_photos('pariz', 3),
  }

  max_count = float(tags[0][1]) if tags else 1
  return render_template('index.html',
          tags = tags,
          max_count = max_count,
          featured = featured,
          android = android)

@app.route("/s/<tag>", methods=['GET'])
def view_stream(tag):
  if not valid_tag(tag): raise NotFound()
  photos = models.get_latest_photos(tag, app.config['MAX_PRELOAD_PHOTOS'])
  return render_template('stream.html', \
                         photos = photos, \
                         tag = tag, \
                         embed = True if request.args.get('embed') else False, \
                         url_root = request.url_root)

@app.route("/s", methods=['POST'])
def add_photo():
  tag = normalize_tag(request.form.get('tag', 'tag'))
  if len(tag) == 0: raise NotFound()
  if 'photo' in request.files:
    # Get data about uploaded photo and save it to the disk
    f = request.files['photo']
    try:
      digest, width, height = photos.process_photo(f,
          save_original = app.config['SAVE_ORIGINAL'])
    except:
      # Something went wrong, just ignore this request
      app.logger.error('Error while processing photo.')
      return redirect(url_for('list_tags'))

    # Get photo's lat/lon
    try:
      lat = float(request.form.get('lat'))
      lon = float(request.form.get('lon'))
    except:
      # Cannot decode or lat/lon is not provided
      lat = None
      lon = None

    # Check if duplicate of the photo exists
    dup = db_session.query(sql.exists().where(
            db.and_(Photo.tag == tag, Photo.digest == digest))).scalar()
    if dup:
      return redirect(url_for('list_tags'))

    # Check if the tag is new
    tag_is_new = not db_session.query(
                       sql.exists().where(Photo.tag == tag)).scalar()

    # Save into database
    photo = Photo(tag, digest, (width, height), (lat, lon))
    db_session.add(photo)
    db_session.commit()

    if (tag_is_new):
      # invalidate cache
      app.logger.info('Invalidate cache.')
      cache.delete_memoized(get_latest_tags)

    # Broadcast to all photo walls
    try:
      send_post(app.config['SOCKETIO_SERVER'] + '/notify', {
        'tag': tag,
        'id': str(photo.id),
        'url': photo_url(digest),
        'host': request.host,
        'width': str(width),
        'height': str(height),
      })
      app.logger.info('Notification sent.')
    except:
      app.logger.error('Failed to notify the stream server.')

    return redirect(url_for('list_tags'))
  else:
    return redirect(url_for('add_photo'))

def photo2map(photo):
  return {
      'src': photo_url(photo.digest),
      'id': photo.id,
      'width': photo.width,
      'height': photo.height,
  }

@app.route("/fresh/<tag>/<int:pid>/")
def fresh_photos(tag, pid):
  if not valid_tag(tag): raise NotFound()
  photos = db_session.query(Photo) \
                     .filter(db.and_(Photo.tag == tag, Photo.id > pid)) \
                     .order_by(Photo.id.desc()) \
                     .limit(app.config['MAX_PRELOAD_PHOTOS']) \
                     .all()
  return jsonify(photos = map(photo2map, photos))

@app.route("/latest/<int:minutes>/")
def latest(minutes):
  time = datetime.timedelta(minutes=minutes)
  return jsonify(latest = models.get_latest_tags(time))

@app.route("/closest/<lat>/<lon>/")
def closest(lat, lon):
  point = (float(lat), float(lon))
  tags = get_latest_tags() # cached function
  tag, distance = utils.closest_tag(point, tags)
  if distance > app.config['MAX_TAG_DISTANCE']:
    return jsonify(tag = None, distance = None)
  else:
    return jsonify(tag = tag, distance = distance)

@app.route("/upload")
def upload_page():
  return render_template('upload.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
