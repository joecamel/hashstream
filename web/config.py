import os

ROOT = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))

DEBUG = True

CACHE_TYPE = 'simple'
CACHE_DEFAULT_TIMEOUT = 60

UPLOADED_PHOTOS_DEST = os.path.join(ROOT, 'static/uploads')
UPLOADED_PHOTOS_URL = '/static/uploads/'
PHOTO_EXTENSIONS = set(['jpg', 'jpeg', 'JPEG', 'JPG'])
THUMBNAIL_SIZE = (160, 160)
PHOTO_MAX_SIZE = (900, 900)
MAX_PRELOAD_PHOTOS = 20
MAX_CONTENT_LENGTH = 3 * 1024 * 1024 # max upload size
SAVE_ORIGINAL = False

import datetime
# Only photos from the last REFRESH_PERIOD time are used for the front-page
# and for hints about tags (we might assume events don't last more than
# this time).
REFRESH_PERIOD = datetime.timedelta(days = 1)

MAX_TAG_DISTANCE = 3000 # in meters

SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/hashstream.db'

SOCKETIO_SERVER = 'http://localhost:8000'

STREAM_HOST = '0.0.0.0'
STREAM_PORT = 8000

if os.path.isfile(os.path.join(ROOT, 'production_config.py')):
  from production_config import *
