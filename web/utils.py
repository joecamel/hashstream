import re, hashlib, base64, urllib, urllib2

# Regex used to validate tags
VALID_TAG = re.compile(r'^[a-zA-Z0-9\-]+$')

def valid_tag(tag):
    return VALID_TAG.match(tag)

def normalize_tag(tag):
    return re.sub('[^a-zA-Z0-9-]', '', tag).strip()

def send_post(url, params, timeout = 3):
    params = urllib.urlencode(params)
    urllib2.urlopen(url, params, timeout)

from math import radians, cos, sin, asin, sqrt

def haversine(lat1, lon1, lat2, lon2):
  """
  Calculates the great circle distance between two points
  on the earth (specified in decimal degrees).
  Returns the result in meters
  """
  # convert decimal degrees to radians 
  lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

  # haversine formula 
  dlon = lon2 - lon1 
  dlat = lat2 - lat1 
  a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
  c = 2 * asin(sqrt(a)) 
  distance = 6367 * 1000 * c # in meters

  return distance

def closest_tag(point, tags):
  # point = (lat, lon)
  if not tags: return (None, None)
  lat1, lon1 = point
  res_tag  = tags[0][0]
  res_dist = haversine(lat1, lon1, tags[0][2], tags[0][3])
  for item in tags:
    tag, _, lat, lon = item
    if isinstance(lat, float) and isinstance(lon, float):
      d = haversine(lat1, lon1, lat, lon)
      if d < res_dist:
        res_dist = d
        res_tag  = tag
  return (res_tag, res_dist)

