import os
import shutil
import hashlib
import Image, ImageOps
from config import UPLOADED_PHOTOS_DEST, UPLOADED_PHOTOS_URL, PHOTO_EXTENSIONS
from config import THUMBNAIL_SIZE, PHOTO_MAX_SIZE

def process_photo(f, save_original=True):
    """Returns (digest, width, height)."""
    if not '.' in f.filename: raise Exception('No extension.')
    ext = f.filename.rsplit('.', 1)[1]
    if not ext in PHOTO_EXTENSIONS: raise Exception('Extension not valid.')

    digest = hashlib.md5(f.read()).hexdigest() # File shouldn't be too big
    f.seek(0)
    filename = digest + '.jpg'
    if os.path.isfile(os.path.join(UPLOADED_PHOTOS_DEST, filename)):
        # file exists, so return tuple
        width, height = Image.open(f).size
        return (digest, width, height)

    directory = UPLOADED_PHOTOS_DEST

    im = Image.open(f)
    width, height = im.size
    if im.format != 'JPEG': raise Exception('Wrong format.')

    f.seek(0)
    # save original file
    if save_original:
      with open(os.path.join(directory, 'orig_' + filename), 'wb') as origf:
        f.save(origf)

    # save image which will be served
    im.thumbnail(PHOTO_MAX_SIZE, Image.ANTIALIAS)
    with open(os.path.join(directory, filename), 'wb') as resizedf:
      im.save(resizedf, 'JPEG')

    # save thumbnail
    im.seek(0)
    im = ImageOps.fit(im, THUMBNAIL_SIZE, Image.ANTIALIAS, (0.5, 0.5))
    with open(os.path.join(directory, 'thumb_' + filename), 'wb') as thumbf:
      im.save(thumbf, 'JPEG')

    return (digest, width, height)

def get_photo_url(digest):
  return UPLOADED_PHOTOS_URL + digest + '.jpg'

def get_thumbnail_url(digest):
  return UPLOADED_PHOTOS_URL + 'thumb_' + digest + '.jpg'

def get_orig_photo_url(digest):
  return UPLOADED_PHOTOS_URL + 'orig_' + digest + '.jpg'
