$(function () {
  var CONNECTED = false, UPDATED=false, LAST_ID = -1;
  var TAG = window.streamid;

  var WEB_SOCKET_SWF_LOCATION = '/static/js/socketio/WebSocketMain.swf',
      socket = io.connect(window.SOCKETIO_SERVER + '/stream');
  
  socket.on('connect', function () {
    socket.emit('join', window.streamid);
    if (window.console) window.console.log("Connected!");
    CONNECTED = true;
  });

  socket.on('reconnect', function () {
    if (window.console) window.console.log("Reconnected!");
    CONNECTED = true;
  });

  socket.on('disconnect', function () {
    if (window.console) window.console.log("Disconnected!");
    CONNECTED = false;
  });

  socket.on('photo', function (id, src, width, height) {
    if (window.console) window.console.log('New photo!', id, src)
    newPhoto({src: src, id: id, width: width, height: height});
    LAST_ID = id;
    UPDATED = true;
  });

  /* Check every 30s if connection is established, if not try
   * to get fresh images! */
  setInterval(function () {
    if (!UPDATED) {
      if (window.console) window.console.log('Asking for fresh photos...');
      $.getJSON('/fresh/' + TAG + '/' + LAST_ID, {}, function (data) {
        if (!data.photos) {
          if (window.console) window.console.log('Nothing fresh there!');
          return;
        }
        var photos = data.photos, i = photos.length;
        while (i--) {
          newPhoto(photos[i]);
          if (photos[i].id > LAST_ID) LAST_ID = photos[i].id;
        }
      });
    }
    UPDATED = false;
  }, 15 * 1000);


  $('#fullscreen-btn').click(function () {
    var $header = $('header').hide().css({
      'position': 'absolute',
      'z-index': 10,
      'width' : '100%'
    });

    doc = document.documentElement;
    fullscreen = doc.requestFullScreen ||
                 doc.mozRequestFullScreen ||
                 doc.webkitRequestFullScreen;
    if (fullscreen) {
      fullscreen.call(doc);
    } else {
      /* Show message to go full screen */
    }

    /* Show header when mouse is near the top edge */;
    var headerVisible = false;
    $(document).mousemove(function (e) {
      if (e.pageY < 30 && !headerVisible) {
        headerVisible = true;
        $header.fadeIn(300);
      } else if (e.pageY > 100 && headerVisible) {
        headerVisible = false;
        $header.fadeOut(300);
      }
    });
  });

  $('#embed').click(function () {
    $(this).fadeOut(300);
  });
  $('#embed textarea').click(function (e) {
    e.preventDefault();
    return false;
  });
  $('#embed-btn').click(function () {
    $('#embed').fadeIn(300);
  });

  $('body').css('overflow', 'hidden');
  $('div.photo').each(function (i, e) {
    var $e = $(e),
        width = $e.data('width'),
        height = $e.data('height'),
        id = parseInt($e.data('id'));
    if (id > LAST_ID) LAST_ID = id;
    if (height > width) $e.css('width', '12.5%');
  });

  var newPhoto = function (data) {
    var $photo = $('<div class="photo">'),
        $photowrapper = $('<div class="photo-wrapper">').appendTo($photo),
        $img = $('<img>').appendTo($photowrapper);
    if (data.height > data.width) {
      $photo.css('width', '12.5%');
    } else {
      $photo.css('width', '25%');
    }
    $img.load(function() {
      addPhoto($photo);
    });
    $img.attr('src', data.src)
  };

  $photos = $('#photos');

  var addPhoto = function (photo) {
    $photos.prepend(photo).masonry('reload');
  };

  setInterval(function () {
    var last = $photos.children('div.photo:last'),
        lastY = last.offset().top,
        lastH = last.height(),
        wHeight = $(window).height();
    if (lastY + lastH * (2/3) > wHeight) {
      addPhoto(last);
    }
  }, 10000);
});

$(window).load(function () {
  $('#photos').masonry({
    columnWidth: function (w) { return w / 8; },
    itemSelector: '.photo',
    isAnimated: true
  });
});
