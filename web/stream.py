import flask
from flask import request, Response
from gevent import monkey
from socketio.virtsocket import Socket
from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin
from socketio import socketio_manage
from socketio.server import SocketIOServer

from utils import normalize_tag

monkey.patch_all()

stream_app = flask.Flask(__name__)
stream_app.config.from_object('config')

class StreamNamespace(BaseNamespace, RoomsMixin):
    def initialize(self):
        self.logger = stream_app.logger
        self.log("Socketio session started.")

    def log(self, message):
        self.logger.info("[{0}] {1}".format(self.socket.sessid, message))

    def on_join(self, room):
        self.room = room
        self.join(room)
        return True

def broadcast_event(server, ns_name, room, event, *args):
    pkt = dict(type="event",
               name=event,
               args=args,
               endpoint=ns_name)
    room = ns_name + '_' + room

    for sessid, socket in server.sockets.iteritems():
      if 'rooms' not in socket.session:
                continue
      print socket.session['rooms']
      stream_app.logger.info('Socket %s, %s' % (str(sessid), room))
      if room in socket.session['rooms']:
        stream_app.logger.info('Sending notification to the client.')
        socket.send_packet(pkt)

create_delegate = stream_app.config.get('CREATE_DELEGATE')
if create_delegate:
  delegate_notification = create_delegate()
else:
  delegate_notification = None

@stream_app.route('/notify', methods=['POST'])
def notify():
  tag = normalize_tag(request.form.get('tag'))
  photoid = long(request.form.get('id'))
  url = request.form.get('url')
  host = request.form.get('host')
  width = int(request.form.get('width'))
  height = int(request.form.get('height'))
  broadcast_event(stream_app.server, '/stream', tag, 'photo',
                  photoid, url, width, height)
  stream_app.logger.info('Received notification for %s: %s' % (tag, url))

  if delegate_notification:
    delegate_notification(tag, host, url)

  return 'NOTIFIED'

# Delegate request to gevent-socketio
@stream_app.route('/socket.io/<path:remaining>')
def socketio(remaining):
    try:
        socketio_manage(request.environ, {'/stream': StreamNamespace}, request)
    except:
        stream_app.logger.error("Exception while handling socketio connection",
                                exc_info=True)
    return Response()

if __name__ == '__main__':
    # Run non-blocking stream app (gevent server)
    print 'Listening on http://0.0.0.0:8000 and on port ' + \
           '10843 (flash policy server)'
    server = SocketIOServer(
        (stream_app.config['STREAM_HOST'], stream_app.config['STREAM_PORT']),
        stream_app, resource="socket.io")
    stream_app.server = server
    server.serve_forever()
