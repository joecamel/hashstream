import sqlalchemy as db
from config import SQLALCHEMY_DATABASE_URI, DEBUG
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import func
import datetime

engine = db.create_engine(SQLALCHEMY_DATABASE_URI, echo=DEBUG)

Base = declarative_base()

class Photo(Base):
    __tablename__ = 'photos'

    id       = db.Column(db.Integer, primary_key=True)
    tag      = db.Column(db.String(20), nullable=False)
    digest   = db.Column(db.String(64), nullable=False)
    time     = db.Column(db.DateTime())
    width    = db.Column(db.Integer)
    height   = db.Column(db.Integer)
    lat      = db.Column(db.Float)
    lon      = db.Column(db.Float)

    def __init__(self, tag, digest, size = (1, 1), pos = (None, None)):
      self.tag      = tag
      self.digest   = digest
      self.time     = datetime.datetime.now()
      self.width, self.height = size
      self.lat, self.lon      = pos

    def __repr__(self):
        return "<Photo('%s', '%s', lat=%f, lon=%f)>" % \
               (self.tag, self.digest, self.lat, self.lon)

def get_latest_photos(tag, limit):
  return db_session.query(Photo) \
     .filter_by(tag = tag) \
     .order_by(Photo.id.desc()) \
     .limit(limit) \
     .all()

def get_latest_tags(timedelta, limit = 100):
  time = datetime.datetime.now() - timedelta
  q = db_session.query(Photo.tag,
                       func.count('*').label('count'),
                       func.avg(Photo.lat),
                       func.avg(Photo.lon)) \
      .filter(Photo.time > time) \
      .group_by(Photo.tag) \
      .order_by('count desc') \
      .limit(limit)
  return q.all()

Base.metadata.create_all(engine)

# Thread-safe session
db_session = scoped_session(sessionmaker(bind=engine, autoflush=False))
