\documentclass[12pt, a4paper]{article}
\usepackage{fullpage}
\usepackage{pdflscape}
\usepackage{caption, subcaption}
\usepackage[kerning,spacing]{microtype}
\usepackage[pdftex]{graphicx}
\usepackage{url}
\usepackage{hyperref}
\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={BEST Code Challenge: Hash Stream},    % title
    pdfnewwindow=true,      % links in new window
    colorlinks=false,       % false: boxed links; true: colored links
    pdfborder=false
}

\begin{document}
\begin{titlepage}
  \begin{center}
    \includegraphics{./logo}\\[1cm]
    {\huge\bfseries Hash Stream}\\[5mm]
    {\large \emph{Hrvoje Bandov}}
  \end{center}

  \vspace{4cm}
  \tableofcontents
\end{titlepage}

\section{Architecture}
\begin{center}
  \vspace{8mm}
  \includegraphics{./arch}\\[1cm]
\end{center}

\noindent
We can distinguish between two types of clients: the ones that upload
new photos (usually mobile clients) and photo walls which show those uploaded
photos.
Photos are uploaded with HTTP POST requests.
The challenging part is to update the photo wall when a new photo is uploaded.
There is a separate server to which web browsers connect via WebSockets
(fallback to Flash Sockets or other techniques: AJAX long polling, 
AJAX multipart streaming, forever Iframe, JSONP Polling).
This ``stream server'' pushes notifications when a new photo arrives.\\[5mm]
\textbf{Important:} the stream server runs on a separate server
(not hosted on Plus) which is against the rules. Of course, the application
will work even without it (fallback to periodical asking for fresh photos)
so you can just ignore it or consider it as an additional feature.\\[5mm]
The stream server is implemented with an event-loop, 
so it's suitable for this task, but it wouldn't work well if it had to do
computationally intensive work (like resizing of photos).
Classical Apache/\texttt{mod\_wsgi} configuration handles everything else.
And of course, in a real-world setting, this would require some sort of efficient
communication between processes/servers.

Database is PostgreSQL and the cache is a simple in-memory cache (configurable for
other possibilities like memcached).
Because there is no support for PostGIS, the database is very simple: one table
which holds photos (MD5 digest, size, geo-position, and a short tag).
Because there is no additional information about tags, there was no need to create
a separate table for them.

Statistics about tags (geo-position, photo count) are made from the photos uploaded
in the last 24 hours (configurable).
Those statistics are cached in memory.
When a client asks for the nearest event, the server goes through the (cached)
statistics and calculates the nearest event. 

\section{Android Application}

There is also a native Android app for uploading photos. It supplements
the mobile web upload page.
It can be launched via ``share intent'' (in gallery or in the photo app) or
from a launcher (then the user has to select a photo).

Because of the lack of time, application is really simple 
(my first Android application).
It uses multipart HTTP POST inside of an AsyncTask to upload the photo.

\noindent It can be downloaded here:
\begin{center}
  \url{http://team55.host25.com/static/hashstream.apk}
\end{center}

\section{Implementation}

The whole project is available on the hosting (\texttt{/home/team55/hashstream/})
and online git repository is also available at BitBucket:
\begin{center}
  \url{https://bitbucket.org/joecamel/hashstream/}\footnote{publicly available
  after the challenge's deadline}
\end{center}
The project is structured as follows:
\begin{itemize}
  \item \texttt{web/} contains everything related to the web application
  \item \texttt{web/app.py} contains views and logic
  \item \texttt{web/models.py} contains database model
  \item \texttt{web/stream.py} contains the stream server code
  \item \texttt{web/static/js} contains JavaScript code
  \item \texttt{web/config.py} contains default configuration
  \item \texttt{android/} contains the Android project
\end{itemize}

Uploading of photos is done over the web. Some browsers on Android
disable uploads and iOS prior to version 6 doesn't support file input
on the web. On the server photos are resized to some smaller resolution.

\subsection{Running the Web Application}
The web application can be simply run with:
\begin{center}
  \texttt{python app.py}
\end{center}
\noindent after all dependencies are satisfied (they are listed in
\texttt{web/requirements.txt}).
They can be downloaded with PyPi.
The stream server is run in the same way: \texttt{python stream.py}.

\subsection{Geolocation and Deja vu}

The web application uses HTML5 Geolocation API on the upload page.
The Android application unfortunately doesn't support geolocation.
Deja vu problem is solved by taking MD5 digest of the uploaded photos
and comparing it later.

\section{Testing}

The web application was tested on all modern browsers (Chrome, Firefox, Safari,
Opera) and Internet Explorer 7, 8, and 9. The application tries to gracefully
degrade and still be usable on older browsers (even without JavaScript).

The upload page is primarily designed for mobile devices, but the user interface
also works well on desktop.

The Android application is tested on Android 2.2 (HTC Desire,
Sony Ericsson Xperia, and Samsung GT-I5800 with 240x400px/146ppi)
and on Android 4.1 (Galaxy Nexus).

\section{Tools and Libraries}

All the tools and libraries used are open and free:
\begin{itemize}
  \item Flask --- a web framework in Python \url{http://flask.pocoo.org/}
  \item Gevent and gevent-socketio --- used for the streaming server
  \url{http://www.gevent.org/}, \url{https://github.com/abourget/gevent-socketio},
  \url{http://socket.io/}
  \item Simple HTML5 bootstrap \url{http://www.initializr.com/}
  \item SQLAlchemy --- for database access \url{http://www.sqlalchemy.org/}
  \item Standard JavaScript libraries: jQuery, Modernizr
  \item Masonry --- library used on the photo wall \url{http://vanilla-masonry.desandro.com/}
  \item SASS --- CSS preprocessor \url{http://sass-lang.com/}
  \item And for the documentation and graphics: \LaTeX, Inkscape, and Gimp
        with icons from \url{http://raphaeljs.com/icons/}
\end{itemize}

\begin{landscape}
\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{7cm}
    \centering
    \includegraphics[width=7cm]{./app-1.png}
    \caption{Native app: share via Hash Stream\\from your gallery or photo app}
  \end{subfigure}%
  \quad
  \begin{subfigure}[b]{7cm}
    \centering
    \includegraphics[width=7cm]{./app-2.png}
    \caption{Native app: photo selected}
  \end{subfigure}%
  \quad
  \begin{subfigure}[b]{7cm}
    \centering
    \includegraphics[width=7cm]{./mobile-upload.png}
    \caption{Mobile web upload\\with geolocation}
  \end{subfigure}%

  \caption{Native app and mobile web}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}[h!]
  \centering
  \includegraphics[width=23cm]{./homepage.png}
  \caption{Homepage}
\end{figure}
\end{landscape}


\end{document}
