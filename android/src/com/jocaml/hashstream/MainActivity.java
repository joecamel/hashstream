package com.jocaml.hashstream;

import java.io.File;
import java.util.regex.Pattern;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final int RESULT_LOAD_IMAGE = 0;
    private File photo;
    private Button upload;
    private EditText tag;
    private TextView message;
    private ProgressDialog progress;
    private static final Pattern validTag = Pattern.compile("^[a-zA-Z0-9_]{1,20}$"); 
    private ConnectivityManager cm;
    private View layout;
	private MainActivity thisActivity;
	
    OnClickListener pickClick;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pickClick = new View.OnClickListener() {
    		@Override
    		public void onClick(View arg0) {
    			if (photo != null) return;
    	        Intent intent = new Intent();
    	        intent.setType("image/*");
    	        intent.setAction(Intent.ACTION_GET_CONTENT);
    	        startActivityForResult(Intent.createChooser(intent,
    	                "Select Picture"), RESULT_LOAD_IMAGE);
    		}
        };
        layout = (View) findViewById(R.id.MainLayout);
        layout.setOnClickListener(pickClick);
        thisActivity = this;
        
        message = (TextView) findViewById(R.id.Message);
        
        tag = (EditText) findViewById(R.id.Tag);
        upload = (Button) findViewById(R.id.Upload);
        upload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String tagname = tag.getText().toString();
            	if (!isConnected()) {
            		toast("You aren't connected to the Internet.");
            	} else if (photo == null) {
            		toast("You didn't choose a photo.");
            	} else if (!validTag.matcher(tagname).matches()) {
            		toast("Only up to 20 characters and digits.");
            	} else {

            		progress = ProgressDialog.show(MainActivity.this, "Uploading",
                            "Please wait...", true);
            		UploadTask task = new UploadTask();
            		PhotoParams params = new PhotoParams(photo, tagname, progress, thisActivity);
            		task.execute(params);
            	}
            }
        });
        
        Intent intent = getIntent();
        String act = getIntent().getAction();
        if (Intent.ACTION_SEND.equals(act)) {
        	Bundle extras = intent.getExtras();
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
                Uri uri = (Uri) extras.getParcelable(Intent.EXTRA_STREAM);
                String path = getPath(uri);
                setUpPhoto(new File(path));
            }
        }
    }
	
	public void setUpPhoto(File photo) {
		this.photo = photo; 
        message.setText(getString(R.string.photo_picked));
	}
	
	public void resetAll() {
		photo = null;
		message.setText(getString(R.string.pick_photo));
		tag.setText("");
	}
	
	private boolean isConnected() {
		if (cm == null) {
			cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		}
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) return false;
		return ni.isConnected();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK) {
			photo = new File(getPath(data.getData()));
			setUpPhoto(photo);
		}
	}

	public String getPath(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	public void toast(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
    
}
