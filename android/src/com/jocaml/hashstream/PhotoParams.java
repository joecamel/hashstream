package com.jocaml.hashstream;

import java.io.File;

import android.app.ProgressDialog;

public class PhotoParams {

    public File photo;
    public String tag;
    public ProgressDialog progress;
    public MainActivity caller;
    
	public PhotoParams(File photo, String tag, ProgressDialog progress, MainActivity caller) {
		super();
		this.photo = photo;
		this.tag = tag;
		this.progress = progress;
		this.caller = caller;
	}
}
