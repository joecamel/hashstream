package com.jocaml.hashstream;

import java.nio.charset.Charset;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.os.AsyncTask;

public class UploadTask extends AsyncTask<PhotoParams, Void, Integer> {

	private ProgressDialog progress;
	private MainActivity caller;
	
	@Override
	protected Integer doInBackground(PhotoParams... params) {
		try {
			progress = params[0].progress;
			caller = params[0].caller;
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("http://team55.host25.com/s"); // HARD CODED!
            MultipartEntity multipartContent =
            		new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, null, Charset.forName("UTF-8"));
            multipartContent.addPart("tag", new StringBody(params[0].tag));
            multipartContent.addPart("photo", new FileBody(params[0].photo));
            
            postRequest.setEntity(multipartContent);
            HttpResponse res = httpClient.execute(postRequest);
            res.getEntity().getContent().close();
            return res.getStatusLine().getStatusCode();
		} catch (Exception e) {
			return 0;
		}
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		if (progress != null) progress.dismiss();
		if (result == 200) {
			caller.resetAll();
			caller.toast("Photo uploaded!");
		} else {
			caller.toast("Failed to upload the photo!");
		}
		super.onPostExecute(result);
	}

}
